let collection = [];

// Write the queue functions below.

	function print(){
		return collection;
	}

	function enqueue(name){
   		 if(collection.length == 0){
        	collection[0] = name;
    	} 
    	else {
        	collection[collection.length] = name;
    	}
    	return collection;
	}

	function dequeue(){ 
		const dequeue = []; 
		for(let i = 0; i < collection.length - 1; i++){ 
			dequeue[i] = collection[i+1]; 
		} 
		collection = dequeue; 
		return collection 
	}

	function front() {
    	return collection[0];
	};

	function size(){
		return collection.length;
	}

	function isEmpty(){
    if(collection.length !== 0){
        return false;
    }else{
        return true;
    }
}

module.exports = {
	collection,
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};